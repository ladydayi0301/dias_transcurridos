const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const indexRoutes = require('./routers/index');
mongoose.connect('mongodb://localhost/dia-transcurrido')
  .then(db => console.log('db connected'))
  .catch(err => console.log(err));


app.set('port', process.env.PORT || 2020);


app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}))

app.use('/', indexRoutes);

app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
  });